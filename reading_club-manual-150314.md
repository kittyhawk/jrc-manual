![Logo](./ss00.png "Readers Against DRM")

* via Tor https://c3jemx2ube5v5zpg.onion/ (Tor browser bundle recommended)
* a Proxy https://c3jemx2ube5v5zpg.onion.city/ (don't use other than for reading/download)

# How to create a book with Jotunbane's Reading Club System

### Introduction

Jotunbane's Reading Club is an online library and publishing system. It was created by Jotunbane. You can read [about his motives](http://c3jemx2ube5v5zpg.onion/?function=about). It's totally free in all aspects and believes in freedom of information. You can [access the hidden service via Tor network](http://c3jemx2ube5v5zpg.onion/).
In case you need help or want to chat use IRC channel #readingclub on OFTC or use other contact data given on the website.

#### How can you contribute?

We are always looking for help. You can contribute mainly in two ways:

1. By creating books for the library using our system
2. If you find 1 too hard, you can at least tell other people about it, thats not so hard.
3. Report problems and solutions to make the process easier for the next one

### The Tools we are using

* a PDF reader
* [Calibre](http://calibre-ebook.com/download)
* [Libre Office](https://www.libreoffice.org/download/) for using the [template](http://c3jemx2ube5v5zpg.onion/reading_club.odt)
  - [AltSearch](http://extensions.libreoffice.org/extension-center/alternative-dialog-find-replace-for-writer)
  - [Pepito Cleaner](http://extensions.libreoffice.org/extension-center/pepito-cleaner)
* Text editor with Search & Replace like Geany (Linux) or Notepad++ (Windows)

### Step 1: Creating the Document Page

At this point, I assume you have signed up for the club. 

* Go to your User Page if not there already by clicking the button on the menu on the left side of the page. 
* At the bottom of your User Page, click the button "New Document".
* A new page will open. Select the Author of the Book from the Dropdown. If the author doesn't exist yet, please write a message to Jotunbane and request the author. You can continue nevertheless and select "Unknown" for now to edit this information later. For now there is no Support for multiple Authors as there was no need yet. The best way to have features like this is to publish a book that needs them. If there is a pseudonym or pen name for an author or even the book has been written by a character of some story, give a note of it in the Colophon. The author shall be represented with a real name as long as the person is known or was revealed after publishing.
* Now select the Subject. The library is based upon [Dewey Decimal Classification](https://en.wikipedia.org/wiki/Dewey_Decimal_Classification). On other sites and online libraries you will see that a book belongs to several genres. Please don't confuse it with genres. A book belongs to only one subject. That subject is its main genre usually. For example, Lord of the Rings belongs to the subject "Fantasy". But it belongs to the genres adventure, magic, classic etc. But the main genre is fantasy. So the subject will be that.
* Most books, don't have a Subtitle. But some do. Subtitle is not the "series" the belongs to. Subtitle is usually written under the title. It maybe seperated by a Colon. For example, A Woman in Berlin: Eight Weeks in the Conquered City. Here "Eight Weeks in the Conquered City" will be the subtitle. Another example, as A Clash of Kings (written under) Book Two of A Game of Thrones. In this case, "Book Two of A Game of Thrones" will not be the subtitle. This refers to the series of the book. It will, therefore be listed in the Colophon section.
* Year is the year of publication of the book. More detailed information, like Month or date and circumstances, about the publication can be given in Colophon. Get it from wikipedia, goodreads, or any other authenticated source.
* Don't mess with Unique ID, just leave it as it is. [More explanation if you want]
* In Keywords, you can enter the other genres that the book belongs to. Other things that the book is about. Some examples might be: wizards, vampires, magical realm etc. Separate each word by a comma. For more examples look at other books in the library.
* In Colophon, follow this pattern:
  - First Published in [Date] by [Publisher]
  - ISBN/ISBN13. Any other reference number.
  - Other important information about the book, that might be relevant.
  - Series: [Name of series #number]
  - Literary Awards(if any)
  - External Links to the book(optional)

Please don't mention any other copyright or publisher information.

* In Teaser, you have to give the summary of the book, description etc. Don't make it too long. Two to three paragraphs max. You can give links to any movie adaption of the book here.
* Next find a good first edition (necessary) cover of the book. You can search for the cover e.g. on [Google image search](http://images.google.com). To prevent too large images (that cannot be rendered by some ebook readers) please limit image size to (this has to be checked!) about 1 MB file size and a width of 800 pixel. Choose the cover image file and then click "Create document".
* Clicking on "Create document" will take you back to your Profile Page. In Documents section you will see the book you just created. (Note, if the book is not visible, you must have missed an important field. Go back and fix it.) Click on the book. A new page will open, this is your book's page on the website. Here you will upload you final prepared document or edit details later. 

Congrats. you have taken the first step. I hope you remain with us till the end of the tutorial.

### Step II: Preparing the Document

Before preparing the document, you should know what kind of format is the document. Right-click the file click properties. The format is written front of "Type of File" These are some of the main formats:

1. PDF aka portable documents format
2. EPUB aka electronic publication (epub)
3. Mobipocket (mobi)
4. DOC aka Word Documents (doc, docx, ...)
5. TXT aka Plaintext (txt, text, asc, md)
6. RTF aka Richtext (rtf)

Here's a screen shot.

![FileProperties](./ss01.png "Windows file properties")

* Now you can see the file type is PDF.
* To create a document you need (Apache) "Open Office" or (the more feature-rich) "Libre Office" and [the template](http://c3jemx2ube5v5zpg.onion/reading_club.odt). You can get them on this links.
* For Apache https://www.openoffice.org/download/index.html
* For Libre http://www.libreoffice.org/download/libreoffice-fresh/
* I prefer Libre Office, so that's what we will talk about for the tutorial.

#### Goal

* To create a formatted Open Document Format (ODT) book from the book you currently have. I will explain shortly how to do the formatting.

First you need to get the template which has built in styles for your convenience. Get it here[link].

* Now you insert your document into the template you just downloaded. You can do it Chapter by Chapter or you can insert the whole file at once.

##### When the source file is PDF

* First try and find another format like EPUB or Mobipocket. PDF should be your last choice.
* For PDF files I usually open them in a PDF reader like Foxit PDF Reader/SumatraPDF (Windows) or evince (Linux) first. Then copy the text and paste it inside the template.
* Alternatively, you can convert them to EPUB using Calibre. After conversion please make sure that the *italics* and **bolds** are intact.

##### When the source file is EPUB

* For EPUB files I convert them to HTML and then insert them (not copy-paste) in the template. To insert a document click Insert on the Main Menu in Libre Office. At the end of the drop down list, select File and the select the HTML file. Same goes for Mobipocket (convert them to HTML etc.)
* To convert an EPUB to HTML you can either use online services or you can use the AVS Document Converter. AVS is only for windows users and is proprietary. It can handle all the major formats. Alternatively, use Calibre. Its both convenient and will manage all your books in one place. Its free and available on all platforms. Using Calibre, convert your books to HTMLZ. HTMLZ is basically a zip file. Open it with a zip utility and you have your HTML file inside it. Note: When you convert a book with images using Calibre to HTMLZ, and then insert it inside the template, the images are only linked. It means whenever you move the book to another place, it will unlink the images so you will only see empty boxes instead of images. The solution is to import all the images one by one.

##### When the source is RTF/HTML

* For RTF and TXT just insert them using Insert->File.

### Step 3: Formatting the document.

Now I assume you have the text inserted into the template by now. If you are still having problems feel free to pm me.
Now we have to format the document. This is very important. We will be using the template. Click on the little tool in the toolbar as shown or press F11:

![Toolbar](./ss02.png "LibreOffice Toolbar")

The following window will open up:

![StyleFormatting](./ss03.png "LibreOffice 'Style and Formatting'")

These are the styles we will be using to format the document. :)
It is important to mention the styles which come with the template. Don't use any other styles besides them. It will only result in errors if you do so. The styles are:
* Book
* BookNoTOC
* Chapter
* ChapterNoTOC
* Footnote
* Head1
* Head2
* Head3
* ListStart
* ListItem
* ListEnd
* OrderedListStart
* OrderedListItem
* OrderedListEnd
* ParaBlankOver
* ParaIndent
* ParaNoIndent
* ParaPreBlankOver
* ParaPreNoIndent
* Part
* PartNoTOC
* QuoteBlankOver
* QuoteIndent
* QuoteNoIndent
* Picture
* TableStart
* TableCell
* TableRow
* TableEnd

Read on to know when to use which style. :)

#### The First Step

The first step, which I consider very very important is to fix the italics. If you don't this will mess up every paragraph containing italics when you upload the book. And then it will take you ages to fix them. Another reason to fix them prior to doing anything is because sometimes when you style a paragraph it will lose all its previous formatting including the italics. This is what we want to avoid. Fixing the italics is the easiest. Get this extension: Alt Search and Replace. Load it in your Libre/Open Office suite.
* In "Search for", click on properties and select italics.
* In Replace, select Character Style. It will open up a small window. Double click on style Emphasis.
* Make sure that the "current selection" check-box is not checked.
* Click "Replace All".
* Italics Fixed! :)

![AltFindReplace](./ss04.png "LibreOffice extension 'Alternative Find and Replace'")

Now you can see below I have the document ready with italics all fixed:

![emphFixed](./ss05.png "LibreOffice showing fixed italiques")

Now the first line is the chapter. We will give it the style chapter. Select all the text of the first line:

![selectedLine](./ss06.png "LibreOffice selected first line")

And double-click "Chapter" in "Style and Formatting" window to make this text as chapter-title.

![markChapter](./ss07.png "LibreOffice use paragraph style for chapter")

It should move to the center and the text size will grow larger. The font will change to Liberation-Serif 16pt. If it doesn't don't worry. Select the text, then right-click and select "Clear direct formatting". To make sure you changed the style. Select the text again and in the "Styles and Formatting" windows "Chapter" **should** be highlighted.
Now sometimes, there is another heading below the chapter heading. Don't make it chapter. Just set it to "Head2".

##### ParaNoIndent, ParaIndent and ParaBlankOver

The starting paragraph of chapter is always set to the style “ParaNoIndent”. Select the first para and do that.

![firstParagraph](./ss08.png "LibreOffice use paragraph style for first paragraph")

Then select all the paragraphs below it until a blank-line occurs. If it doesn't keep selecting the paragraphs until the start of the next chapter. Change all these paragraphs to the style "ParaIndent".
If a Blank-line occurs, then stop there. Change all these paragraphs to the style "ParaIndent".

The paragraph just after the blank line, select it, and change it's style to "ParaBlankOver". Make sure to remove the blank line as "ParaBlankOver" will insert its own blank line. All the paragraphs after that will go to style "ParaIndent" again. Blank-line example:

![nextParagraphs](./ss09.png "LibreOffice use paragraph style for more paragraphs")

Same goes for all the chapters. Easy, eh?

##### Head1, Head2 and Head3

Use your source document to find out when to use which heading. These are headings that are not shown in the ToC.

"Head1" is used for a major heading.
The paragraph following "Head1" will be given the style "ParaNoIndent".

"Head2" for normal headings. It may be used for a heading that comes under "Head1".
The paragraph following "Head1" will be given the style "ParaBlankOver".

"Head3" is rarely used. It may be used for a heading that comes under "Head2".
The paragraph following "Head1" will be given the style "ParaBlankOver".

##### Pictures

* For the pictures, insert a picture. Then right-click the picture and select Anchor->As Charachter. Once you have done that. Select the picture as if you were selecting text. Change the picture style to "Picture".

##### Embedding already linked pictures

After you inserted the text from e.g. a RTF or an HTML-file pictures also are included als links but have to be embedded. For being more effective and not doing this by hand, you may use this inbuild function of LibreOffice to resolve the links:

1. Edit / Links
2. Select the pictures in the list
3. Click the button "Break Link"
4. Save the document

##### QuoteNoIndent, QuoteBlankOver, QuoteIndent

* For Quotations, use the style "QuoteIndent" or "QuoteNoIndent". Whichever suits you best.
* Quotations are used for: quotes, poems, letters, Something special mentioned etc.
* If the quote or poem is in between two paragraphs use the style QuoteBlankOver. The paragraph following it will be given the style "ParaBlankOver".

##### Part

* If the book is divided into parts, and parts into chapters. Select the part-text e.g "Part I" and change it to "Part".

##### Book

If the book is divided into multiple books, use the Style "Book" for the title of each Book. For example see the books in the library under the subject "Collections".

##### ChapterNoTOC

For things like Dedication, Acknowledgments, Epigraph or a chapter that is not going to be listed in the Table of Contents(TOC), use the style "ChapterNoTOC".

##### ParaPreBlankOver and ParaPreNoIndent

These two styles are used, on paragraphs which are one the following:
* Text Messages
* Email
* Source Code
* The text was written in the Mono font already.

##### How to create a table

Creating a table is quite easy. Suppose I found this in a document:
	Book		Author
	LOTR		Tolkien
	Art of War	Sun-Tzu

Now this is okay, but I want it in a table. How do I do it? Simple. Go to Insert->Table in the Main-Menu.

![createTable](./ss10.png "LibreOffice insert table")

I have 3-rows and 2 columns. The table is here:
Book	Author
LOTR	Tolkien
The Art of War	Sun Tzu

##### Determine keyboard shortcuts for styles

LibreOffice Writer is able to set custom keyboard shortcuts to work faster in your hands. Here we show how to configure the maybe most used styles (ParaBlankOver and ParaIndent) as en example.
But you may also use this for any other available function of this powerful word processor.

1. Open from the Menu bar: "Tools" / "Customize", the dialoque opening is called "Customize"
2. Change to the tab "Keyboard"
3. Inside the area "Functions" you find "Category" for functions, scroll on the very bottom of that list
4. Expand "Styles" with the plus-button and select "Paragraph" to show all styles for paragraphs in the list "Function", sorted by name alphabetically
5. Scroll down to the ParaGraphs of your intrest, here we assume "ParaBlankOver" first
6. Now choose the desired shortcut from the list labeled "Shortcut keys" in the top area of the dialoque and select it with the button "Modify"
7. Check if the selected shortcut has been added to the list "Keys" in the bottom area.
8. Repeat (from #5) for style "ParaIndent" and maybe others you will need. To save the changes simply hit the button "OK".

Now you can use your keybindings to format the text much faster than by clicking with your mouse/touch-devices.

#### Checking the Document

Once you have done the whole document, you need to check for errors. Go to Edit->Find&Replace or press Ctrl-H. Select "Other Options".  Check "Search for Paragraph Styles". (Shortcut: Alt + O, Alt + y.) And then see if there are any foreign styles in the document like: text-body or Default Text etc.

![findReplaceParagraphStyles](./ss11.png "LibreOffice 'Find and Replace' for Paragraph Styles")

#### Correcting the errors

Once you have cleared the document. Upload it. If you have done it carefully and checked everything and done everything as mentioned you will get no errors. If this is the case, Click Let's see it. This will take you to the document page. Check again to make sure you have all the chapters and everything is okay. If everything is fine click "Publish". Voila! Its done.
If there are errors such as P1, P2... P7 etc. Click Let's see it. This will take you to the document page. This is important before reuploading a new version: Click "Flush". Go back and fix the errors in your source document. If you are finding difficulty in fixing the errors, read more [#about fixing errors in XML] or just ask. Flushing the document is important as this will prevent duplications. When the document is finally fixed. Upload it.

#### about fixing errors in XML

In case you have errors on upload, most probably the automatic subsidizing of (LibreOffice) Writer is not yet supported that well by the system. Then you can simply first make sure you did everything from the above section [#Correcting the errors]. Then if the errors remains still, save the ODT as FODT. This means, that it will not be compressed (technically as ZIP) but remain as one single editable XML document that you can edit with standard tools. Start your text editor to do so. One supporting undo/redo, search & replace with regular expressions as well as syntax highlighting can make your live easier a lot.
Then search for the names shown by the error message. E.g. if there was an error involving P1, search for "P1". The first time it will be declared similar to a style you want to have set, e.g. "ParaBlankOver". Then replace all following matches for "P1" with "ParaBlankOver". Before you continue, test if the document still opens without problems in Writer while you still can undo your changes in the editor. Then proceed to the next error.

There are several things you can remove as well.
Just to name a few: soft page breaks (<text:soft-page-break/>), unreferenced bookmarks or hyperlink-anchors, unwanted hyperlinks.
You might find out more while experimenting with the XML part. At least basic knowledge of HTML is helping a lot here.

#### Final Note

That's it. If you are still having problems, pm me. I will help you out.
If you have any suggestions for the manual, pm me. For suggestions to th system, pm Jotunbane. Or even better, contact via the IRC channel.
Use keybindings (aka shortcuts) for styles as discribed in [determine shortcuts for styles] - it will help a lot and you will be a lot faster.
When you have done more than 10 books. PM me. I will show you more advanced ways of accomplishing the task.

Love, KittyHawk :)